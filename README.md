# Custumizations for bash programs

Run command in bash to copy config files.

    bash <(curl -s https://gitlab.com/matjash/dotfiles/raw/master/makeConfigs.sh)
    
# Install ZSH

Install script

    bash <(sudo curl -s https://gitlab.com/matjash/dotfiles/raw/master/ZSH.sh)
    
Complete uninstall ZSH and config files

    sudo apt-get purge zsh -y \
    && rm -rf ~/.z ~/.zsh_history ~/.zshrc ~/.zsh-update ~/.oh-my-zsh \
    && find ~/ -name ".zcompdump*" -exec rm -f {} \; \
    && sudo groupdel chsh \
    && sudo sed -i '1d' /etc/pam.d/chsh && sudo sed -i '1d' /etc/pam.d/chsh
    
>   Only run full uninstall command if installation was succesfful because it
>   delete's first two lines of /etc/pam.d/chsh/. 
    
If error on upgrade use:

    cd "$ZSH" && git stash && upgrade_oh_my_zsh
    
>   Problem is with modified template files files, on list to repair.

# Install ZSH v2

Install script

    bash <(sudo curl -s https://gitlab.com/matjash/dotfiles/raw/master/zsh2.sh)
    
Complete uninstall ZSH and config files

    sudo apt-get purge zsh -y \
    && rm -rf ~/.z ~/.zsh_history ~/.zshrc ~/.zsh-update ~/.oh-my-zsh ~/.zshextra \
    && find ~/ -name ".zcompdump*" -exec rm -f {} \; \
    && sudo groupdel chsh \
    && sudo sed -i '1d' /etc/pam.d/chsh && sudo sed -i '1d' /etc/pam.d/chsh
    
>   Only run full uninstall command if installation was succesfful because it
>   delete's first two lines of /etc/pam.d/chsh/. 