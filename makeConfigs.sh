#!/bin/sh
echo Script will copy config files to "/home/$USER/" folder, press y to list available files!
read answer

function copyConfig () {
  url=https://gitlab.com/matjash/dotfiles/raw/master/$answer
  if [[ ! -f ~/$answer ]]; then
    curl -s $url >> ~/$answer
    echo
    echo ~/$answer created
    echo
  else
    echo
    echo File already exists, append config to file!
    read yes
    echo
     if [[ $yes = "y" ]]; then
      curl -s $url >> ~/$answer
      echo Config appended!
     fi
  fi
  # If source is needed else don't show stderr!
  source ~/$answer > /dev/null 2>&1
  echo
  echo Copy another config y/n?
  read answer
}

function list () {
  curl https://gitlab.com/matjash/dotfiles/raw/master/filelist
}

while [[ $answer = "y" ]]; do
  list
  echo
  read answer
  if [[ $answer != "" ]]; then
    copyConfig $answer
  fi
done

echo
echo Script finished!