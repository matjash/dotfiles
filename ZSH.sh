#!/bin/sh
echo "Script will install ZSH bash with oh-myzsh template + zsh-syntax-highlighting + z.sh"
read xxxxxxxxx

# Install ZSH
sudo apt-get install zsh

# Allow user to change bash without sudo
# Add lines to top of /etc/pam.d/chsh
sudo sed -i '1s/^/auth       sufficient   pam_wheel.so trust group=chsh \n /' /etc/pam.d/chsh
sudo sed -i '1s/^/# This allows users of group chsh to change their shells without a password.\n /' /etc/pam.d/chsh
sudo groupadd chsh
sudo usermod -a -G chsh $USER

# Add oh-my-zsh template
curl -L http://install.ohmyz.sh | sh
# Add zsh-syntax-highlighting
cd ~/.oh-my-zsh && git clone git://github.com/zsh-users/zsh-syntax-highlighting.git 
# Add z.sh last paths - https://github.com/rupa/z/blob/master
sudo curl https://raw.githubusercontent.com/rupa/z/master/z.sh >> ~/.oh-my-zsh/z.sh

# Configure .zshrc file
## Change template
sed -i "s,robbyrussell,mh,g" ~/.zshrc

## Enable zsh-syntax-highlighting
echo "" >> ~/.zshrc
echo "# My configuration" >> ~/.zshrc
echo 'source ${ZSH}/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh' >> ~/.zshrc
echo "# Z path" >> ~/.zshrc
echo '. ${ZSH}/z.sh' >> ~/.zshrc
echo "# Aliases" >> ~/.zshrc
echo "alias ll=\"ls -la\"" >> ~/.zshrc
echo "alias -g NULL=\"> /dev/null 2>&1\""
sed -i 's,# CASE_SENSITIVE="true",CASE_SENSITIVE="true",g' ~/.zshrc
# sed -i 's,# HYPHEN_INSENSITIVE="true",HYPHEN_INSENSITIVE="true",g' ~/.zshrc
sed -i 's,# DISABLE_AUTO_TITLE="true",DISABLE_AUTO_TITLE="true",g' ~/.zshrc
# sed -i 's,# ENABLE_CORRECTION="true",ENABLE_CORRECTION="true",g' ~/.zshrc

# Customize theme
sed -i 's,fg\[red\],fg\[yellow\],g' ~/.oh-my-zsh/themes/mh.zsh-theme
# Add machine name
sed -i 's,}:%{$fg\[,}@%m:%{$fg\[,g' ~/.oh-my-zsh/themes/mh.zsh-theme
sed -i 's,$UID -eq 0,$UID -eq 1000,g' ~/.oh-my-zsh/themes/mh.zsh-theme

echo "To change bash to zsh on login type \"y\"..."
read yes
if [[ ${yes} = "y" ]]; then
    # Change shell
    binZSH=$(which zsh)
    echo "ZSH path is: $binZSH"
    chsh -s $binZSH
    echo "OK :)"
    zsh
else 
    binBASH=$(which bash)
    chsh -s $binBASH
    echo "OK :)"
    zsh
fi

